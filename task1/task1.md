# Semestrální práce 1

Vytvoril som si na openstacku dve virtualky.

Jedno ako ctl (12 GB vcc2.large) a dalsie ako cmp (4GB vcc1.small).

ctl - 10.119.71.199

cmp - 10.119.70.145



#### ctl:
```
sudo apt update
```
```
sudo apt upgrade
```
```
sudo apt-get install python3-dev libffi-dev gcc libssl-dev
```

```
ubuntu@ctl:~$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/ubuntu/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/ubuntu/.ssh/id_rsa
Your public key has been saved in /home/ubuntu/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:OVDd4ioNCVnRE5b07aKiHAgmO7CBVa5eP7seb8kamTM ubuntu@ctl
The key's randomart image is:
+---[RSA 3072]----+
|    .oo+=+ .     |
|   oo  o+.o..    |
|  . .... o...    |
|.. .  o. ...     |
|*.. .  oS.. .    |
|+* o ..ooo .     |
|+ o . Eo..       |
| . . o X+        |
|    o.=+.        |
+----[SHA256]-----+

ubuntu@ctl:~$ cat /home/ubuntu/.ssh/id_rsa.pub >> /home/ubuntu/.ssh/authorized_keys

ubuntu@ctl:~$ cat /home/ubuntu/.ssh/id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCdtF660noS6Gv2nI2VkQ9EcNN10S/Rb4FvW4zsLYtTiykMZGpY7+wa2XTne31e4ffLwa2wqUq/UBJChl+m5iECtJniabeP9dmD15JziIoEkIKKG9gvKwi+Rb5Wnbptcug4iMIJ2c40gGmV17E2VLgzBCjCrr5ZjNn28Hwphc6gSnmI0ZWO0ExwQ1mqHgQ0YVTsb+RXCctVzsWWJwqJCOK8eNdmIuj1IBRoY/vEBuiU+JB1w6Asa/V5A0pBCpqrTfIKYPCM70thV0TC0S5SZRAa2y27tqO6gjJPHKd/BFT+ZY90S8vceBlvBV2eVGH18n0FNvqNuwdE7xlhmw/20rD5PxjCFPi82ks6WQW5d2G4BJ4NpR19aVZpPx9P1QiZKguVHBRBSr5w9W0yd2WlME1jmnedTRLKyYj5z9mPHiYZ5yErzb3lcjzGjw7G+LQx7G2iAlYnCz9WKna4v24K6uF6oVi4QrZwLNVFa+RwOyIW106z7NB/7V/rDNYRc0fz3qE= ubuntu@ctl
```

#### cmp:

nakopirujem kluc z ctl na cmp

```
ubuntu@cmp:~$ nano /home/ubuntu/.ssh/authorized_keys
```

Nainstalujeme prerekvizity
```
sudo apt update
sudo apt upgrade
sudo apt install python3-dev libffi-dev gcc libssl-dev
sudo apt install python3-pip
pip3 install -U pip
```

#### ctl:

```
ubuntu@ctl:~$ ssh 10.119.70.145
The authenticity of host '10.119.70.145 (10.119.70.145)' can't be established.
ED25519 key fingerprint is SHA256:CGrlA7zKEaf3dumEj8ynM77vUyJzYyBAy9YIhOtMoA8.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.119.70.145' (ED25519) to the list of known hosts.
Welcome to Ubuntu 22.04.4 LTS (GNU/Linux 5.15.0-1059-kvm x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/pro

 System information as of Tue May 28 21:15:43 UTC 2024

  System load:  0.28               Processes:             74
  Usage of /:   10.9% of 19.20GB   Users logged in:       1
  Memory usage: 4%                 IPv4 address for ens3: 10.119.70.145
  Swap usage:   0%

 * Strictly confined Kubernetes makes edge and IoT secure. Learn how MicroK8s
   just raised the bar for easy, resilient and secure K8s cluster deployment.

   https://ubuntu.com/engage/secure-kubernetes-at-the-edge

Expanded Security Maintenance for Applications is not enabled.

0 updates can be applied immediately.

Enable ESM Apps to receive additional future security updates.
See https://ubuntu.com/esm or run: sudo pro status

Last login: Tue May 28 21:10:32 2024 from 10.38.36.87
```

Prihlasili sme ako cmp takze prepojenie medzi funguje.


Nainstalujeme prerekvizity
```
sudo apt install python3-pip
sudo pip3 install -U pip
sudo pip install -U 'ansible>=4,<6'
sudo pip3 install git+https://opendev.org/openstack/kolla-ansible@unmaintained/zed
```

Nastavime kola ansible:
```
ubuntu@ctl:~$ sudo mkdir -p /etc/kolla
ubuntu@ctl:~$ sudo chown $USER:$USER /etc/kolla
ubuntu@ctl:~$ cp -r /usr/local/share/kolla-ansible/etc_examples/kolla/* /etc/kolla
ubuntu@ctl:~$ cp /usr/local/share/kolla-ansible/ansible/inventory/* .
ubuntu@ctl:~$ git clone --branch unmaintained/zed https://opendev.org/openstack/kolla-ansible
ubuntu@ctl:~$ sudo mkdir /etc/ansible
ubuntu@ctl:~$ sudo nano /etc/ansible/ansible.cfg
```

Nasledne som nastavil:

```
[defaults]
host_key_checking=False
pipelining=True
forks=100
```

Nasledne nastavime ako by mali uzly pracovat:
```
ubuntu@ctl:~$ sudo nano multinode
```

Nasledne upravíme súbor multinode:


| Sekce         | Akce                                                                                                         |
|---------------|--------------------------------------------------------------------------------------------------------------|
| [control]     | - Zmažeme "control01,02,03"<br>- Nakopírujeme tam IP adresu= USERNAME-ctl (USERNAME je ten co mam na cloude) |
| [compute]     | - Nakopírujeme ip adresu=USERNAME-cmp a a podobne pre ctl <br>- Zmažeme "compute01"                          |
| [network]     | - Upravíme na [network:children]<br>- Umažeme "network01,02"<br>- Připíšeme tam "control"                    |
| [monitoring]  | - Upravíme na [monitoring:children]<br>- Umažeme "monitoring01"<br>- Připíšeme tam "control"                 |
| [storage]     | - Upravíme na [storage:children]<br>- Umažeme "storage01"<br>- Připíšeme tam "compute"                       |


```
ubuntu@ctl:~$ ansible -i multinode all -m ping
...
[WARNING]: Invalid characters were found in group names but not replaced, use -vvvv to see details
10.119.70.145 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
localhost | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
10.119.71.199 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
```
```
ubuntu@ctl:~$ kolla-genpwd
WARNING: Passwords file "/etc/kolla/passwords.yml" is world-readable. The permissions will be changed.
```

Následne upravíme soubor globals.yml:
```
nano /etc/kolla/globals.yml
```

| Sekce                   | Akce                                                                                                        |
|-------------------------|-------------------------------------------------------------------------------------------------------------|
| [control]               | - Zmažeme "control01,02,03"<br>- Nakopírujeme tam IP adresu USERNAME-ctl (USERNAME je ten co mam na cloude) |
| [compute]               | - Nakopírujeme adresu USERNAME-cmp a USERNAME-ctl<br>- Zmažeme "compute01"                                  |
| [network]               | - Upravíme na [network:children]<br>- Umažeme "network01,02"<br>- Připíšeme tam "control"                   |
| [monitoring]            | - Upravíme na [monitoring:children]<br>- Umažeme "monitoring01"<br>- Připíšeme tam "control"                |
| [storage]               | - Upravíme na [storage:children]<br>- Umažeme "storage01"<br>- Připíšeme tam "compute"                      |
| Kontejnery              | - Odkomentovat `kolla_base_distro` a přepsat `centos` na `ubuntu`<br>- Toto změní distribuci v kontejnerech |
| OpenStack Release       | - Odkomentovat `openstack_release`<br>- Zafixovat na `"zed"`                                                |
| Kolla Internal VIP      | - Odkomentovat `kolla_internal_vip_address`<br>- Přepsat IP adresu na IP adresu USERNAME-ctl                |
| Network Interface       | - Odkomentovat `network_interface` v sekci Neutron<br>- Přepsat `eth0` na `ens3`                            |
| Neutron External Interface | - Odkomentovat `neutron_external_interface`<br>- Prepisat `eth1` na `veth0`                                 |
| OpenStack Options       | - Odkomentovat `enable_haproxy` v sekci Openstack options<br>- Prepisat `yes` na `no`                       |



#### PRE ctl aj cmp:
```
ubuntu@ctl:~$ sudo nano /usr/local/bin/setup-veth.sh
```

A pridal som:
```
#!/bin/bash

ip link add veth0 type veth peer name veth1
ip link set veth0 up
ip link set veth1 up
```

Nasledne som dal prava a nastavil interface:
```
ubuntu@ctl:~$ sudo chmod +x /usr/local/bin/setup-veth.sh
ubuntu@ctl:~$ sudo mkdir -p /etc/networkd-dispatcher/routable.d
ubuntu@ctl:~$ sudo ln -s /usr/local/bin/setup-veth.sh /etc/networkd-dispatcher/routable.d/setup-veth.sh
ubuntu@ctl:~$ sudo netplan apply
```

```
ubuntu@ctl:~$ ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: ens3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether fa:16:3e:86:0c:14 brd ff:ff:ff:ff:ff:ff
    inet 10.119.71.199/22 metric 100 brd 10.119.71.255 scope global dynamic ens3
       valid_lft 86377sec preferred_lft 86377sec
    inet6 fe80::f816:3eff:fe86:c14/64 scope link
       valid_lft forever preferred_lft forever
3: veth1@veth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether ca:a9:50:dd:49:26 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::c8a9:50ff:fedd:4926/64 scope link
       valid_lft forever preferred_lft forever
4: veth0@veth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether aa:e7:7f:fa:ab:cd brd ff:ff:ff:ff:ff:ff
    inet6 fe80::a8e7:7fff:fefa:abcd/64 scope link
       valid_lft forever preferred_lft forever
```

```
ubuntu@cmp:~$ ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: ens3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether fa:16:3e:05:e1:64 brd ff:ff:ff:ff:ff:ff
    inet 10.119.70.145/22 metric 100 brd 10.119.71.255 scope global dynamic ens3
       valid_lft 86390sec preferred_lft 86390sec
    inet6 fe80::f816:3eff:fe05:e164/64 scope link
       valid_lft forever preferred_lft forever
3: veth1@veth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 1a:5f:4c:f3:16:cb brd ff:ff:ff:ff:ff:ff
    inet6 fe80::185f:4cff:fef3:16cb/64 scope link
       valid_lft forever preferred_lft forever
4: veth0@veth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 16:5c:1d:19:f4:78 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::44a4:aaff:fe09:911b/64 scope link
       valid_lft forever preferred_lft forever
```


Aby sme vedeli spustat VM na viacerych uzloch este nam treba aby virtualka vedela si spravit svoju adresu

#### ctl:
Nainstalujeme na server prerekvizity

```
ubuntu@ctl:~$ ansible-galaxy collection install -r /usr/local/share/kolla-ansible/requirements.yml
```

```

ubuntu@ctl:~$ kolla-ansible bootstrap-servers -i multinode

...
PLAY RECAP *************************************************************************************************************************************************************************************************************************************************************************************************************
10.119.70.145              : ok=30   changed=12   unreachable=0    failed=0    skipped=23   rescued=0    ignored=0
10.119.71.199              : ok=30   changed=11   unreachable=0    failed=0    skipped=23   rescued=0    ignored=0
localhost                  : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

```
ubuntu@ctl:~$ kolla-ansible prechecks -i multinode
=====> Vela vystupov ale tiez vsetky boli ok, zabudol som ich zapisat

```

```
ubuntu@ctl:~$ kolla-ansible deploy -i multinode  <====== vytvara kontajnery, mariadb stuff, configy...

...
PLAY [Apply role venus] ************************************************************************************************************************************************************************************************************************************************************************************************
skipping: no hosts matched

PLAY RECAP *************************************************************************************************************************************************************************************************************************************************************************************************************
10.119.70.145              : ok=68   changed=48   unreachable=0    failed=0    skipped=51   rescued=0    ignored=0
10.119.71.199              : ok=276  changed=193  unreachable=0    failed=0    skipped=127  rescued=0    ignored=1
localhost                  : ok=4    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

ubuntu@ctl:~$ sudo apt install python3-openstackclient
ubuntu@ctl:~$ kolla-ansible post-deploy
PLAY RECAP *************************************************************************************************************************************************************************************************************************************************************************************************************
localhost                  : ok=5    changed=3    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0
```

Vyzera ze vsetko prebehlo v poriadku.

Nechám si vypisat credentials pre nas openstack:
```
ubuntu@ctl:~$ cat /etc/kolla/admin-openrc.sh
```

```
# Ansible managed

# Clear any old environment that may conflict.
for key in $( set | awk '{FS="="}  /^OS_/ {print $1}' ); do unset $key ; done
export OS_PROJECT_DOMAIN_NAME='Default'
export OS_USER_DOMAIN_NAME='Default'
export OS_PROJECT_NAME='admin'
export OS_TENANT_NAME='admin'
export OS_USERNAME='admin'
export OS_PASSWORD='4KqoiIJkkBY5ELUWajMN3lClAy0POngvanKaPh62'
export OS_AUTH_URL='http://10.119.71.199:5000'
export OS_INTERFACE='internal'
export OS_ENDPOINT_TYPE='internalURL'
export OS_IDENTITY_API_VERSION='3'
export OS_REGION_NAME='RegionOne'
export OS_AUTH_PLUGIN='password'
```


Napojil som sa na net kde som zadal ip adresu ctl.
Pripojil som sa ako username: admin a heslo je v cat /etc/kolla/admin-openrc.sh - OS_USERNAME a OS_PASSWORD
Teraz uz vieme sa napojit na openstack a vieme vytvarat nove instance.

![Diagram](img/logged to openstack.PNG)

Tu by mala koncit 2. úloha.


## úloha 3 - OpenStack, který dokáže spouštět VM na dvou uzlech (buď druhý výpočetní nebo služba compute na řídícím uzlu). VM musí na sebe dokázat pingovat.:
Nastavime pre security group ALL TCP na skolskom openstacku aj na openstacku ktory sme vytvorili my.

![Diagram](img/tcp.PNG)


Oba prikazy spustime na CTL node na ktorej uz bezi openstack.

Zavolame prikazy ktore nam daju admin credentials aby sme mohli vytvorit instancie:

```
ubuntu@ctl:~$ . /etc/kolla/admin-openrc.sh
ubuntu@ctl:~$ /usr/local/share/kolla-ansible/init-runonce
```

Tento prikaz spusti instanciu na ctl uzle.

```
openstack server create \
    --image cirros \
    --flavor m1.tiny \
    --key-name mykey \
    --network demo-net \
    demo1
```

Tento prikaz spusti instanciu na cmp uzle. Pre cmp uzol som specifikoval availability-zone nova:cmp pri demo1sa to defaultne vytvorila pre ctl

```
openstack server create \
    --image cirros \
    --flavor m1.tiny \
    --key-name mykey \
    --network demo-net \
    --availability-zone nova:cmp \
    demo2
```


Instancie:

![Diagram](img/instances.PNG)
![Diagram](img/adming-instancie.PNG)


Ping instancie demo 1 do demo2
![Diagram](img/demo1to2.PNG)


Ping instancie demo 2 do demo1
![Diagram](img/demo2to1.PNG)

Dokazeme sa pignut medzi instanciami takze vsetko funguje ako ma.

## úloha 4 - Jako v minulém bodě, plus funkční přístup z VM do Internetu pomocí NAT.:

Nastavime net.ipv4.ip_forward = 1 
```
ubuntu@ctl:~$ sudo nano /etc/sysctl.conf
```

```
ubuntu@ctl:~$ sudo sysctl -p
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-iptables = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.neigh.default.gc_thresh1 = 128
net.ipv4.neigh.default.gc_thresh2 = 28672
net.ipv4.neigh.default.gc_thresh3 = 32768
net.ipv6.neigh.default.gc_thresh1 = 128
net.ipv6.neigh.default.gc_thresh2 = 28672
net.ipv6.neigh.default.gc_thresh3 = 32768
```

```
ubuntu@ctl:~$ sudo ip a add 10.0.2.1/24 brd + dev veth1
ubuntu@ctl:~$ sudo iptables -t nat -A POSTROUTING -s 10.0.2.0/24 ! -d 10.0.2.0/24 -j MASQUERADE
```

Nasledne vyskusame ci instancie sa vedia dostat von:


![Diagram](img/demo1pingnet.PNG)
![Diagram](img/demo2pingnet.PNG)

Vyzera ze obe instancie sa vedia dostat von. Takze vsetko funguje ok.








