# Semestrální práce 3


V tejto ulohe sme pouzili tento dockerfile:

```
# Use an official Python runtime as a parent image
FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive
ENV WORKERS=8

RUN apt-get update -y \
    && apt-mark hold locales \
    && apt-get install -y python3-pip python3.10-venv git build-essential \
    && apt-mark unhold locales \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /requirements

COPY requirements /requirements

RUN python3 -m pip install --upgrade pip \
    && pip install --no-cache-dir -r /requirements/requirements.txt

# Copy the current directory contents into the container at /usr/src/app
WORKDIR /usr/src/app
COPY . .

# Create logs directory
RUN mkdir -p /usr/src/app/logs

# Install package in editable mode
RUN SETUPTOOLS_SCM_PRETEND_VERSION=0.0.1 pip install -e .

# Go to app directory
WORKDIR /usr/src/app/app

## Run main.py
CMD uvicorn app.main:app --host 0.0.0.0 --port ${RESTAPI_PORT} --workers ${WORKERS}
```

Ide o Fast API python aplikaciu


### 1. úloha - Sestavení obrazu s nějakou webovou aplikací metodou Dockerfile. Pro inspiraci použít návod na Wordpress ze cvičení, ale vzít jinou aplikaci, např. semestrálku z jiného předmětu.

```
docker build -t call-service .
```


![Diagram](results/uloha1.PNG)


```
kedar@DESKTOP-42QS30H:/mnt/d/SchoolProjects/VCC/3$ docker image list
REPOSITORY     TAG       IMAGE ID       CREATED             SIZE
call-service   latest    429f615c3c48   About an hour ago   518MB
ubuntu         22.04     52882761a72a   3 weeks ago         77.9MB
```

### 2. úloha - Analýza obrazu ve formátu OCI/Docker získaného z předchozího kroku pomocí "docker save". Které vrstvy odpovídají podkladovému obrazu a které příkazům z Dockerfile?

Uložim a vyextrahujem base image aj image pre aplikaciu:

```
mkdir app-docker-dir
mkdir base-image

docker save call-service  -o call-service.tar
tar -xvf call-service.tar -C app-docker-dir

docker save ubuntu -o ubuntu.tar
tar -xvf ubuntu.tar  -C base-image
```

Vsetky layers som extrahoval z taru do suborov.

Následne som ich diffol a zistil ze mi pribudlo 8 adresarov z toho layer 629ca62fb7c791374ce57626d6b8b62c76378be091a0daf1a60d32700b49add7 je spolocny pre base image and app image:

```
diff app-docker-dir/blobs/sha256/ base-image/blobs/sha256/
Only in app-docker-dir/blobs/sha256/: 021cae3d9890ff1ab078304c9feffaf58610efa272eaf53ad6e1fddb9e25209e~
Only in app-docker-dir/blobs/sha256/: 27a672ae09b1d79dfd6408ba44c591ec6c760cccf7942d554d0b4667965b3d6e~
Only in app-docker-dir/blobs/sha256/: 42513e1a5278891e0bb883608fb1cebf154e151c0f27150d376b1a8a172fbab5~
Only in app-docker-dir/blobs/sha256/: 4656837d1bd64241ddb9fdb9a0846632d3050f3043b74395b09a44334c58e3f2~
Only in app-docker-dir/blobs/sha256/: 4b9affcaa74067081ce4b538d63838473b2277ca42718cb89e29c17d098a21e5~
Common subdirectories: app-docker-dir/blobs/sha256/629ca62fb7c791374ce57626d6b8b62c76378be091a0daf1a60d32700b49add7~ and base-image/blobs/sha256/629ca62fb7c791374ce57626d6b8b62c76378be091a0daf1a60d32700b49add7~
Only in app-docker-dir/blobs/sha256/: 9146870885d8887854bac4ec72ea5613f2aedda833e5e888f629ca5d7347fcea~
Only in app-docker-dir/blobs/sha256/: aa4f33b7aa0574f4aa5e5df958f5f9afc0fb5183287f8017b3a48cda76f78b70~
Only in app-docker-dir/blobs/sha256/: eac1a447aad028c31dfe65975740376b365435822d5a67d0b1316ac7ad06f64e~
```


Nechal som si vypisat layere pre app image:
Mozme vidiet ze prvy layer je ten ktory je spolocny s base imagom. 

```
629ca62fb7c791374ce57626d6b8b62c76378be091a0daf1a60d32700b49add7
```

```
kedar@DESKTOP-42QS30H:/mnt/d/SchoolProjects/VCC/3/app-docker-dir$ cat manifest.json | jq
[
  {
    "Config": "blobs/sha256/429f615c3c486035aad1d73216e31729a2522ce057905ed991016b5a1b96afed",
    "RepoTags": [
      "call-service:latest"
    ],
    "Layers": [
      "blobs/sha256/629ca62fb7c791374ce57626d6b8b62c76378be091a0daf1a60d32700b49add7",
      "blobs/sha256/eac1a447aad028c31dfe65975740376b365435822d5a67d0b1316ac7ad06f64e",
      "blobs/sha256/42513e1a5278891e0bb883608fb1cebf154e151c0f27150d376b1a8a172fbab5",
      "blobs/sha256/4656837d1bd64241ddb9fdb9a0846632d3050f3043b74395b09a44334c58e3f2",
      "blobs/sha256/4b9affcaa74067081ce4b538d63838473b2277ca42718cb89e29c17d098a21e5",
      "blobs/sha256/9146870885d8887854bac4ec72ea5613f2aedda833e5e888f629ca5d7347fcea",
      "blobs/sha256/27a672ae09b1d79dfd6408ba44c591ec6c760cccf7942d554d0b4667965b3d6e",
      "blobs/sha256/aa4f33b7aa0574f4aa5e5df958f5f9afc0fb5183287f8017b3a48cda76f78b70",
      "blobs/sha256/021cae3d9890ff1ab078304c9feffaf58610efa272eaf53ad6e1fddb9e25209e",
      "blobs/sha256/5f70bf18a086007016e948b04aed3b82103a36bea41755b6cddfaf10ace3c6ef"
    ],
    "LayerSources": {
      "sha256:021cae3d9890ff1ab078304c9feffaf58610efa272eaf53ad6e1fddb9e25209e": {
        "mediaType": "application/vnd.oci.image.layer.v1.tar",
        "size": 1472512,
        "digest": "sha256:021cae3d9890ff1ab078304c9feffaf58610efa272eaf53ad6e1fddb9e25209e"
      },
      "sha256:27a672ae09b1d79dfd6408ba44c591ec6c760cccf7942d554d0b4667965b3d6e": {
        "mediaType": "application/vnd.oci.image.layer.v1.tar",
        "size": 15037440,
        "digest": "sha256:27a672ae09b1d79dfd6408ba44c591ec6c760cccf7942d554d0b4667965b3d6e"
      },
      "sha256:42513e1a5278891e0bb883608fb1cebf154e151c0f27150d376b1a8a172fbab5": {
        "mediaType": "application/vnd.oci.image.layer.v1.tar",
        "size": 1536,
        "digest": "sha256:42513e1a5278891e0bb883608fb1cebf154e151c0f27150d376b1a8a172fbab5"
      },
      "sha256:4656837d1bd64241ddb9fdb9a0846632d3050f3043b74395b09a44334c58e3f2": {
        "mediaType": "application/vnd.oci.image.layer.v1.tar",
        "size": 4608,
        "digest": "sha256:4656837d1bd64241ddb9fdb9a0846632d3050f3043b74395b09a44334c58e3f2"
      },
      "sha256:4b9affcaa74067081ce4b538d63838473b2277ca42718cb89e29c17d098a21e5": {
        "mediaType": "application/vnd.oci.image.layer.v1.tar",
        "size": 57353216,
        "digest": "sha256:4b9affcaa74067081ce4b538d63838473b2277ca42718cb89e29c17d098a21e5"
      },
      "sha256:5f70bf18a086007016e948b04aed3b82103a36bea41755b6cddfaf10ace3c6ef": {
        "mediaType": "application/vnd.oci.image.layer.v1.tar",
        "size": 1024,
        "digest": "sha256:5f70bf18a086007016e948b04aed3b82103a36bea41755b6cddfaf10ace3c6ef"
      },
      "sha256:629ca62fb7c791374ce57626d6b8b62c76378be091a0daf1a60d32700b49add7": {
        "mediaType": "application/vnd.oci.image.layer.v1.tar",
        "size": 80412672,
        "digest": "sha256:629ca62fb7c791374ce57626d6b8b62c76378be091a0daf1a60d32700b49add7"
      },
      "sha256:9146870885d8887854bac4ec72ea5613f2aedda833e5e888f629ca5d7347fcea": {
        "mediaType": "application/vnd.oci.image.layer.v1.tar",
        "size": 2560,
        "digest": "sha256:9146870885d8887854bac4ec72ea5613f2aedda833e5e888f629ca5d7347fcea"
      },
      "sha256:aa4f33b7aa0574f4aa5e5df958f5f9afc0fb5183287f8017b3a48cda76f78b70": {
        "mediaType": "application/vnd.oci.image.layer.v1.tar",
        "size": 3072,
        "digest": "sha256:aa4f33b7aa0574f4aa5e5df958f5f9afc0fb5183287f8017b3a48cda76f78b70"
      },
      "sha256:eac1a447aad028c31dfe65975740376b365435822d5a67d0b1316ac7ad06f64e": {
        "mediaType": "application/vnd.oci.image.layer.v1.tar",
        "size": 379093504,
        "digest": "sha256:eac1a447aad028c31dfe65975740376b365435822d5a67d0b1316ac7ad06f64e"
      }
    }
  }
]
```


Nasledne sme zistili co robi kazda vrstva:

```
"629ca62fb7c791374ce57626d6b8b62c76378be091a0daf1a60d32700b49add7",
"eac1a447aad028c31dfe65975740376b365435822d5a67d0b1316ac7ad06f64e",
"42513e1a5278891e0bb883608fb1cebf154e151c0f27150d376b1a8a172fbab5",
"4656837d1bd64241ddb9fdb9a0846632d3050f3043b74395b09a44334c58e3f2",
"4b9affcaa74067081ce4b538d63838473b2277ca42718cb89e29c17d098a21e5",
"9146870885d8887854bac4ec72ea5613f2aedda833e5e888f629ca5d7347fcea",
"27a672ae09b1d79dfd6408ba44c591ec6c760cccf7942d554d0b4667965b3d6e",
"aa4f33b7aa0574f4aa5e5df958f5f9afc0fb5183287f8017b3a48cda76f78b70",
"021cae3d9890ff1ab078304c9feffaf58610efa272eaf53ad6e1fddb9e25209e",
"5f70bf18a086007016e948b04aed3b82103a36bea41755b6cddfaf10ace3c6ef"
```


#### Vrstva 629ca62fb7c791374ce57626d6b8b62c76378be091a0daf1a60d32700b49add7: 
```
kedar@DESKTOP-42QS30H:/mnt/d/SchoolProjects/VCC/3/app-docker-dir/blobs/sha256$ tree 629ca62fb7c791374ce57626d6b8b62c76378be091a0daf1a60d32700b49add7~

Output bol tu prilis dlhy a to bolo preto leb to vytvorilo subory systemu.
```


#### Vrstva eac1a447aad028c31dfe65975740376b365435822d5a67d0b1316ac7ad06f64e:
```
kedar@DESKTOP-42QS30H:/mnt/d/SchoolProjects/VCC/3/app-docker-dir/blobs/sha256$ tree eac1a447aad028c31dfe65975740376b365435822d5a67d0b1316ac7ad06f64e~
Output je taktiez dlhy lebo pouzivame sudo apt update a upgrade ktory nainstaloval strasne vela veci a preto je velky output

-- Tato vrstva nainstalovala rozne balicky. 
```


```
Odpoveda tomuto argumentu: RUN apt-get update -y \
    && apt-mark hold locales \
    && apt-get install -y python3-pip python3.10-venv git build-essential \
    && apt-mark unhold locales \
    && rm -rf /var/lib/apt/lists/*
```



#### Vrstva 42513e1a5278891e0bb883608fb1cebf154e151c0f27150d376b1a8a172fbab5:
#### - Tato vrstva nakopirovala requirements subory do vnutra zlozky.

```
kedar@DESKTOP-42QS30H:/mnt/d/SchoolProjects/VCC/3/app-docker-dir/blobs/sha256$ tree 42513e1a5278891e0bb883608fb1cebf154e151c0f27150d376b1a8a172fbab5~/
42513e1a5278891e0bb883608fb1cebf154e151c0f27150d376b1a8a172fbab5~/
└── requirements

-- Tato vrstva vytvorila requirements zlozku


1 directory, 0 files
kedar@DESKTOP-42QS30H:/mnt/d/SchoolProjects/VCC/3/app-docker-dir/blobs/sha256$ tree 4656837d1bd64241ddb9fdb9a0846632d3050f3043b74395b09a44334c58e3f2~/
4656837d1bd64241ddb9fdb9a0846632d3050f3043b74395b09a44334c58e3f2~/
└── requirements
    ├── requirements-dev.txt
    ├── requirements-test.txt
    └── requirements.txt
```




#### Vrstva 4b9affcaa74067081ce4b538d63838473b2277ca42718cb89e29c17d098a21e5:
####  - Vytvorila directory app
```
kedar@DESKTOP-42QS30H:/mnt/d/SchoolProjects/VCC/3/app-docker-dir/blobs/sha256$ tree 4b9affcaa74067081ce4b538d63838473b2277ca42718cb89e29c17d098a21e5~/

-- Tato vrstva ma taktiez obrovsky output a to preto lebo upgradla pip a nainstalovala balicky.


kedar@DESKTOP-42QS30H:/mnt/d/SchoolProjects/VCC/3/app-docker-dir/blobs/sha256$ tree 9146870885d8887854bac4ec72ea5613f2aedda833e5e888f629ca5d7347fcea~/
9146870885d8887854bac4ec72ea5613f2aedda833e5e888f629ca5d7347fcea~/
└── usr
    └── src
        └── app
```




#### Vrstva 27a672ae09b1d79dfd6408ba44c591ec6c760cccf7942d554d0b4667965b3d6e:
####  - Nakopirovala projekt

```
kedar@DESKTOP-42QS30H:/mnt/d/SchoolProjects/VCC/3/app-docker-dir/blobs/sha256$ tree 27a672ae09b1d79dfd6408ba44c591ec6c760cccf7942d554d0b4667965b3d6e~/
27a672ae09b1d79dfd6408ba44c591ec6c760cccf7942d554d0b4667965b3d6e~/
└── usr
    └── src
        └── app
            ├── Dockerfile
            ├── Dockerfile-Test-Image
            ├── README.md
            ├── app
            │   ├── __init__.py
            │   ├── __pycache__
            │   │   ├── __init__.cpython-310.pyc
            │   │   └── setup_logging.cpython-310.pyc
            │   ├── config
            │   │   ├── __init__.py
            │   │   ├── __pycache__
            │   │   │   ├── __init__.cpython-310.pyc
            │   │   │   ├── config.cpython-310.pyc
            │   │   │   └── config_dict.cpython-310.pyc
            │   │   ├── config.py
            │   │   └── config_dict.py
            │   ├── dependencies
            │   │   ├── __pycache__
            │   │   │   ├── call_event_handler.cpython-310.pyc
            │   │   │   ├── chatbot_interaction.cpython-310.pyc
            │   │   │   └── text_utils.cpython-310.pyc
            │   │   ├── call_event_handler.py
            │   │   ├── chatbot_interaction.py
            │   │   └── text_utils.py
            │   ├── main.py
            │   ├── setup_logging.py
            │   └── tryout.py
            ├── call_service_automation.egg-info
            │   ├── PKG-INFO
            │   ├── SOURCES.txt
            │   ├── dependency_links.txt
            │   ├── requires.txt
            │   └── top_level.txt
            ├── config.yaml
            ├── devtunnel.exe
            ├── pyproject.toml
            ├── requirements
            │   ├── requirements-dev.txt
            │   ├── requirements-test.txt
            │   └── requirements.txt
            ├── setup.py
            └── tests
                ├── __init__.py
                ├── __pycache__
                │   └── __init__.cpython-310.pyc
                ├── e2e
                └── unit
                    ├── __init__.py
                    ├── __pycache__
                    │   ├── __init__.cpython-310.pyc
                    │   ├── config_test.cpython-310-pytest-8.1.0.pyc
                    │   ├── logging_test.cpython-310-pytest-8.1.0.pyc
                    │   └── test_logging.cpython-310-pytest-8.1.0.pyc
                    ├── config_test.py
                    ├── data
                    │   └── config
                    │       ├── test.env
                    │       └── testing_config.yaml
                    └── logging_test.py
```



#### Vrstva aa4f33b7aa0574f4aa5e5df958f5f9afc0fb5183287f8017b3a48cda76f78b70:
#### - Vytvorila log folder
```
kedar@DESKTOP-42QS30H:/mnt/d/SchoolProjects/VCC/3/app-docker-dir/blobs/sha256$ tree aa4f33b7aa0574f4aa5e5df958f5f9afc0fb5183287f8017b3a48cda76f78b70~/
aa4f33b7aa0574f4aa5e5df958f5f9afc0fb5183287f8017b3a48cda76f78b70~/
└── usr
    └── src
        └── app
            └── logs
```




#### Vrstva 021cae3d9890ff1ab078304c9feffaf58610efa272eaf53ad6e1fddb9e25209e:
#### - Nainstalovala balicek lokalne v editable mode.
```
kedar@DESKTOP-42QS30H:/mnt/d/SchoolProjects/VCC/3/app-docker-dir/blobs/sha256$ tree 021cae3d9890ff1ab078304c9feffaf58610efa272eaf53ad6e1fddb9e25209e~/
021cae3d9890ff1ab078304c9feffaf58610efa272eaf53ad6e1fddb9e25209e~/
├── root
├── tmp
└── usr
    ├── local
    │   └── lib
    │       └── python3.10
    │           └── dist-packages
    │               ├── __editable__.call_automation_service-0.0.1.pth
    │               ├── __editable___call_automation_service_0_0_1_finder.py
    │               ├── __pycache__
    │               │   └── __editable___call_automation_service_0_0_1_finder.cpython-310.pyc
    │               └── call_automation_service-0.0.1.dist-info
    │                   ├── INSTALLER
    │                   ├── METADATA
    │                   ├── RECORD
    │                   ├── REQUESTED
    │                   ├── WHEEL
    │                   ├── direct_url.json
    │                   └── top_level.txt
    └── src
        └── app
            └── call_automation_service.egg-info
                ├── PKG-INFO
                ├── SOURCES.txt
                ├── dependency_links.txt
                ├── requires.txt
                └── top_level.txt

12 directories, 15 files
```


### 3. úloha - Vytvoření systémového kontejneru s jinou distribucí než Debian a Ubuntu. Pro inspiraci lze použít systemd-nspawn předvedený na cvičení.

Vytvorim repozitar pre fedoru a nainstalujem fedoru do kontaineru.

```
sudo mkdir -p /var/lib/machines/fedora-container
```

Vytvorime minimalny Fedora system

```commandline
sudo dnf install -y systemd-container
sudo dnf -y --installroot=/var/lib/machines/fedora-container --releasever=38 --disablerepo='*' --enablerepo=fedora install systemd passwd dnf
sudo cp /etc/resolv.conf /var/lib/machines/fedora-container/etc/
```

Tymto prikazom spustim kontainer.
```
radek@radek-Lenovo-ideapad-100-15IBY:~$ sudo systemd-nspawn -b -D /var/lib/machines/fedora-container
```

Zistime PID hlavneho procesu kontaineru a pomocou nsenter sa dostanene do kontajneru a zistime PID 1 proces.
```
radek@radek-Lenovo-ideapad-100-15IBY:~$ sudo machinectl show fedora-container -p Leader
[sudo] password for radek: 
Leader=56944
radek@radek-Lenovo-ideapad-100-15IBY:~$ sudo machinectl show fedora-container -p Leader
Leader=56944
radek@radek-Lenovo-ideapad-100-15IBY:~$ sudo nsenter -t 56944 -p
root@radek-Lenovo-ideapad-100-15IBY:/home/radek# ps -p 1 -o comm=
systemd
root@radek-Lenovo-ideapad-100-15IBY:/home/radek#
```

Nasledne si to mozme overit prikazom ps -p 1 v samotnej instancii kontaineru.
```
myuser@radek-Lenovo-ideapad-100-15IBY:~$ ps -p 1
    PID TTY          TIME CMD
      1 ?        00:00:00 systemd
```



### 4. úloha - Napsání jednotkového souboru pro Systemd (unit file), který v "systemd-analyze security" bude mít alespoň žluté skóre zabezpečení. Ideálně pro nějakou semestrální práci z jiného předmětu, jejímž výstupem je nějaká síťová/webová služba, nebo vyjít z návodů v literatuře (dobrá služba na pokusy je třeba tinyproxy).
V tomto kroku som uz nepouzival wsl2 ale pouzil som Linuxovy systém.

Napisanie systemdunit suboru:


```
[Unit]
Description=Call service app.

[Service]
ExecStart=/usr/bin/python3.10 /home/radek/School/call-automation-service/app/ma>

[Install]
WantedBy=multi-user.target
```


#### Aktualizujeme daemon s naším unit file


```
systemctl daemon-reload
```


#### Spustime sluzbu
```
systemctl start call_service.service
```


```
radek@radek-Lenovo-ideapad-100-15IBY:~$ systemd-analyze security call_service.service
...
→ Overall exposure level for call_service.service: 9.6 UNSAFE 😨
```
#### V unit file som urobil tieto upravy:

```
[Unit]
Description=Call service app.

[Service]
ExecStart=/usr/bin/python3.10 /home/radek/School/call-automation-service/app/ma>

ProtectSystem=strict
ProtectKernelTunables=true
ProtectControlGroups=true
ProtectKernelModules=true
CapabilityBoundingSet=

[Install]
WantedBy=multi-user.target
```

```
radek@radek-Lenovo-ideapad-100-15IBY:~$ systemd-analyze security call_service.service
  NAME                                                        DESCRIPTION      >
✗ PrivateNetwork=                                             Service has acces>
✗ User=/DynamicUser=                                          Service runs as r>
✓ CapabilityBoundingSet=~CAP_SET(UID|GID|PCAP)                Service cannot ch>
✓ CapabilityBoundingSet=~CAP_SYS_ADMIN                        Service has no ad>
✓ CapabilityBoundingSet=~CAP_SYS_PTRACE                       Service has no pt>
✗ RestrictAddressFamilies=~AF_(INET|INET6)                    Service may alloc>
✗ RestrictNamespaces=~CLONE_NEWUSER                           Service may creat>
✗ RestrictAddressFamilies=~…                                  Service may alloc>
✓ CapabilityBoundingSet=~CAP_(CHOWN|FSETID|SETFCAP)           Service cannot ch>
✓ CapabilityBoundingSet=~CAP_(DAC_*|FOWNER|IPC_OWNER)         Service cannot ov>
✓ CapabilityBoundingSet=~CAP_NET_ADMIN                        Service has no ne>
✓ CapabilityBoundingSet=~CAP_SYS_MODULE                       Service cannot lo>
✓ CapabilityBoundingSet=~CAP_SYS_RAWIO                        Service has no ra>
✓ CapabilityBoundingSet=~CAP_SYS_TIME                         Service processes>
✗ DeviceAllow=                                                Service has no de>
✗ IPAddressDeny=                                              Service does not >
✓ KeyringMode=                                                Service doesn't s>
✗ NoNewPrivileges=                                            Service processes>
✓ NotifyAccess=                                               Service child pro>
✗ PrivateDevices=                                             Service potential>
✓ PrivateMounts=                                              Service cannot in>
✗ PrivateTmp=                                                 Service has acces>
✗ PrivateUsers=                                               Service has acces>
✗ ProtectClock=                                               Service may write>
✓ ProtectControlGroups=                                       Service cannot mo>
✗ ProtectHome=                                                Service has full >
✗ ProtectKernelLogs=                                          Service may read >
✓ ProtectKernelModules=                                       Service cannot lo>
✓ ProtectKernelTunables=                                      Service cannot al>
✗ ProtectProc=                                                Service has full >
✓ ProtectSystem=                                              Service has stric>
✗ RestrictAddressFamilies=~AF_PACKET                          Service may alloc>
✗ RestrictSUIDSGID=                                           Service may creat>
✗ SystemCallArchitectures=                                    Service may execu>
✗ SystemCallFilter=~@clock                                    Service does not >
✗ SystemCallFilter=~@debug                                    Service does not >
✗ SystemCallFilter=~@module                                   Service does not >
✗ SystemCallFilter=~@mount                                    Service does not >
✗ SystemCallFilter=~@raw-io                                   Service does not >
✗ SystemCallFilter=~@reboot                                   Service does not >
✗ SystemCallFilter=~@swap                                     Service does not >
✗ SystemCallFilter=~@privileged                               Service does not >
✗ SystemCallFilter=~@resources                                Service does not >
✓ AmbientCapabilities=                                        Service process d>
✓ CapabilityBoundingSet=~CAP_AUDIT_*                          Service has no au>
✓ CapabilityBoundingSet=~CAP_KILL                             Service cannot se>
✓ CapabilityBoundingSet=~CAP_MKNOD                            Service cannot cr>
✓ CapabilityBoundingSet=~CAP_NET_(BIND_SERVICE|BROADCAST|RAW) Service has no el>
✓ CapabilityBoundingSet=~CAP_SYSLOG                           Service has no ac>
✓ CapabilityBoundingSet=~CAP_SYS_(NICE|RESOURCE)              Service has no pr>
✗ RestrictNamespaces=~CLONE_NEWCGROUP                         Service may creat>
✗ RestrictNamespaces=~CLONE_NEWIPC                            Service may creat>
✗ RestrictNamespaces=~CLONE_NEWNET                            Service may creat>
✗ RestrictNamespaces=~CLONE_NEWNS                             Service may creat>
✗ RestrictNamespaces=~CLONE_NEWPID                            Service may creat>
✗ RestrictRealtime=                                           Service may acqui>
✗ SystemCallFilter=~@cpu-emulation                            Service does not >
✗ SystemCallFilter=~@obsolete                                 Service does not >
✗ RestrictAddressFamilies=~AF_NETLINK                         Service may alloc>
✗ RootDirectory=/RootImage=                                   Service runs with>
  SupplementaryGroups=                                        Service runs as r>
✓ CapabilityBoundingSet=~CAP_MAC_*                            Service cannot ad>
✓ CapabilityBoundingSet=~CAP_SYS_BOOT                         Service cannot is>
✓ Delegate=                                                   Service does not >
✗ LockPersonality=                                            Service may chang>
✗ MemoryDenyWriteExecute=                                     Service may creat>
  RemoveIPC=                                                  Service runs as r>
✗ RestrictNamespaces=~CLONE_NEWUTS                            Service may creat>
✗ UMask=                                                      Files created by >
✓ CapabilityBoundingSet=~CAP_LINUX_IMMUTABLE                  Service cannot ma>
✓ CapabilityBoundingSet=~CAP_IPC_LOCK                         Service cannot lo>
✓ CapabilityBoundingSet=~CAP_SYS_CHROOT                       Service cannot is>
✗ ProtectHostname=                                            Service may chang>
✓ CapabilityBoundingSet=~CAP_BLOCK_SUSPEND                    Service cannot es>
✓ CapabilityBoundingSet=~CAP_LEASE                            Service cannot cr>
✓ CapabilityBoundingSet=~CAP_SYS_PACCT                        Service cannot us>
✓ CapabilityBoundingSet=~CAP_SYS_TTY_CONFIG                   Service cannot is>
✓ CapabilityBoundingSet=~CAP_WAKE_ALARM                       Service cannot pr>
✗ RestrictAddressFamilies=~AF_UNIX                            Service may alloc>
✗ ProcSubset=                                                 Service has full >

→ Overall exposure level for call_service.service: 6.2 MEDIUM 😐
```

Služba ma security rating 6.2 MEDIUM





