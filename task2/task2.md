# Semestrální práce 2


## úloha 1.

###  Nalezení XML deskriptoru a v něm jména rozhraní

Na cmp node vlezieme do nova-libvirt kontajneru a zistíme nazov inštancie a nasledne interface:


```
ubuntu@cmp:~$ sudo docker exec -it nova_libvirt bash -i
(nova-libvirt)[root@cmp /]# virsh list
 Id   Name                State
-----------------------------------
 2    instance-00000003   running

(nova-libvirt)[root@cmp /]# cat /etc/libvirt/qemu/instance-00000003.xml
...
<interface type='bridge'>
  <mac address='fa:16:3e:b6:81:f7'/>
  <source bridge='qbr7c19394b-94'/>
  <target dev='tap7c19394b-94'/>
  <model type='virtio'/>
  <mtu size='1450'/>
  <address type='pci' domain='0x0000' bus='0x00' slot='0x03' function='0x0'/>
</interface>
```

Nazov rozhrania je `tap7c19394b-94`

### Trasování provozu mezi 2 VM
Následne urobime ping z demo1 na demo2 pomocou:

Demo2 ip: `10.0.0.173`

```commandline
ping 10.0.0.173
```

Pokial beži ping, tak urobime tcpdump na rozhranie `tap7c19394b-94`:

```commandline
tcpdump -i tap7c19394b-94
```

```commandline
ubuntu@cmp:~$ sudo tcpdump -i tap7c19394b-94
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on tap7c19394b-94, link-type EN10MB (Ethernet), snapshot length 262144 bytes
23:20:22.366406 IP 10.0.0.151 > 10.0.0.173: ICMP echo request, id 32116, seq 51, length 64
23:20:22.367181 IP 10.0.0.173 > 10.0.0.151: ICMP echo reply, id 32116, seq 51, length 64
23:20:22.550011 ARP, Request who-has 10.0.0.173 tell 10.0.0.151, length 28
23:20:22.550495 ARP, Reply 10.0.0.173 is-at fa:16:3e:b6:81:f7 (oui Unknown), length 28
23:20:23.371003 IP 10.0.0.151 > 10.0.0.173: ICMP echo request, id 32116, seq 52, length 64
23:20:23.374419 IP 10.0.0.173 > 10.0.0.151: ICMP echo reply, id 32116, seq 52, length 64
23:20:24.373802 IP 10.0.0.151 > 10.0.0.173: ICMP echo request, id 32116, seq 53, length 64
23:20:24.374590 IP 10.0.0.173 > 10.0.0.151: ICMP echo reply, id 32116, seq 53, length 64
23:20:25.378317 IP 10.0.0.151 > 10.0.0.173: ICMP echo request, id 32116, seq 54, length 64
23:20:25.378985 IP 10.0.0.173 > 10.0.0.151: ICMP echo reply, id 32116, seq 54, length 64
23:20:26.381518 IP 10.0.0.151 > 10.0.0.173: ICMP echo request, id 32116, seq 55, length 64
23:20:26.383832 IP 10.0.0.173 > 10.0.0.151: ICMP echo reply, id 32116, seq 55, length 64
23:20:27.384706 IP 10.0.0.151 > 10.0.0.173: ICMP echo request, id 32116, seq 56, length 64
23:20:27.386568 IP 10.0.0.173 > 10.0.0.151: ICMP echo reply, id 32116, seq 56, length 64
23:20:28.387467 IP 10.0.0.151 > 10.0.0.173: ICMP echo request, id 32116, seq 57, length 64
23:20:28.388345 IP 10.0.0.173 > 10.0.0.151: ICMP echo reply, id 32116, seq 57, length 64
23:20:29.392649 IP 10.0.0.151 > 10.0.0.173: ICMP echo request, id 32116, seq 58, length 64
23:20:29.394363 IP 10.0.0.173 > 10.0.0.151: ICMP echo reply, id 32116, seq 58, length 64
^C
18 packets captured
18 packets received by filter
0 packets dropped by kernel
```


## úloha 2. - Trasování provozu tunelovaného pomocí VXLAN mezi dvěma uzly, a to zobrazené v grafickém nástroji Wireshark


```commandline
ubuntu@cmp:~$ sudo tcpdump -n -i ens3 udp -w data_dump
tcpdump: listening on ens3, link-type EN10MB (Ethernet), snapshot length 262144 bytes
^C30 packets captured
32 packets received by filter
0 packets dropped by kernel
```

#### Wireshark screenshot:

![Diagram](2/wireshark.PNG)


##  úloha 3. - Nakreslit diagram síťových prvků, přes které prochází east-west provoz (mezi dvěma VM na různých hypervizorech), a to s konkrétními jmény rozhraní ve Vaši instalaci OpenStacku (doloženo namátkově screenshoty).


```commandline
ubuntu@cmp:~$ ip link | tail
12: vxlan_sys_4789: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 65000 qdisc noqueue master ovs-system state UNKNOWN mode DEFAULT group default qlen 1000
    link/ether 92:bc:4a:a3:85:de brd ff:ff:ff:ff:ff:ff
13: qbr7c19394b-94: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue state UP mode DEFAULT group default qlen 1000
    link/ether 4e:10:4c:ea:dd:b7 brd ff:ff:ff:ff:ff:ff
14: qvo7c19394b-94@qvb7c19394b-94: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master ovs-system state UP mode DEFAULT group default qlen 1000
    link/ether 36:02:09:00:55:0e brd ff:ff:ff:ff:ff:ff
15: qvb7c19394b-94@qvo7c19394b-94: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master qbr7c19394b-94 state UP mode DEFAULT group default qlen 1000
    link/ether d6:fc:63:ed:ca:d0 brd ff:ff:ff:ff:ff:ff
16: tap7c19394b-94: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc pfifo_fast master qbr7c19394b-94 state UNKNOWN mode DEFAULT group default qlen 1000
    link/ether 16:f2:3a:2c:f8:b3 brd ff:ff:ff:ff:ff:ff
```

```commandline
ubuntu@ctl:~$ ip link | tail
21: vxlan_sys_4789: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 65000 qdisc noqueue master ovs-system state UNKNOWN mode DEFAULT group default qlen 1000
    link/ether 26:7c:cd:44:a4:6d brd ff:ff:ff:ff:ff:ff
22: qbre55749bf-a2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue state UP mode DEFAULT group default qlen 1000
    link/ether c2:e5:8b:9c:58:f6 brd ff:ff:ff:ff:ff:ff
23: qvoe55749bf-a2@qvbe55749bf-a2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master ovs-system state UP mode DEFAULT group default qlen 1000
    link/ether 02:ed:84:8b:3f:e9 brd ff:ff:ff:ff:ff:ff
24: qvbe55749bf-a2@qvoe55749bf-a2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master qbre55749bf-a2 state UP mode DEFAULT group default qlen 1000
    link/ether ce:be:b8:30:76:ec brd ff:ff:ff:ff:ff:ff
25: tape55749bf-a2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc pfifo_fast master qbre55749bf-a2 state UNKNOWN mode DEFAULT group default qlen 1000
    link/ether fe:16:3e:86:bc:76 brd ff:ff:ff:ff:ff:ff
```


![Diagram](2/network.PNG)


##  úloha 4. 
### Trasování provozu služby poskytující instancím při startu metadata na adrese 169.254.169.254 od instance přes metadata-proxy až k Nova-API